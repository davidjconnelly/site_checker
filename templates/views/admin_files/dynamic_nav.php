<a class="w3-bar-item w3-button w3-hover-black" href="<?= BASE_URL ?>daily_checks/manage"><i class="fa fa-calendar-check-o"></i> Daily Checks</a>
<a class="w3-bar-item w3-button w3-hover-black" href="<?= BASE_URL ?>enquiries/manage"><i class="fa fa-envelope"></i> Manage Enquiries</a>
<a class="w3-bar-item w3-button w3-hover-black" href="<?= BASE_URL ?>target_webpages/manage"><i class="fa fa-file-code-o"></i> Target Webpages</a>
<a class="w3-bar-item w3-button w3-hover-black" href="<?= BASE_URL ?>rankings/manage"><i class="fa fa-tag"></i> Manage Rankings</a>