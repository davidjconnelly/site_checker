-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 23, 2020 at 01:31 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `site_checker`
--

-- --------------------------------------------------------

--
-- Table structure for table `adverts`
--

CREATE TABLE `adverts` (
  `id` int(11) NOT NULL,
  `date_found` date DEFAULT NULL,
  `advert_url` varchar(255) DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `target_webpage_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `associated_target_webpages_and_daily_checks`
--

CREATE TABLE `associated_target_webpages_and_daily_checks` (
  `id` int(11) NOT NULL,
  `target_webpages_id` int(11) NOT NULL,
  `daily_checks_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `comment` text DEFAULT NULL,
  `date_created` int(11) DEFAULT 0,
  `user_id` int(11) DEFAULT NULL,
  `target_table` varchar(125) DEFAULT NULL,
  `update_id` int(11) DEFAULT NULL,
  `code` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `daily_checks`
--

CREATE TABLE `daily_checks` (
  `id` int(11) NOT NULL,
  `date_checked` date DEFAULT NULL,
  `target_webpage_id` int(11) NOT NULL DEFAULT 0,
  `checked` tinyint(1) NOT NULL DEFAULT 0,
  `num_adverts_found` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `daily_checks`
--

INSERT INTO `daily_checks` (`id`, `date_checked`, `target_webpage_id`, `checked`, `num_adverts_found`) VALUES
(1, '2020-11-23', 43, 0, 0),
(2, '2020-11-23', 44, 0, 0),
(3, '2020-11-23', 42, 0, 0),
(4, '2020-11-23', 13, 0, 0),
(5, '2020-11-23', 33, 0, 0),
(6, '2020-11-23', 15, 0, 0),
(7, '2020-11-23', 35, 0, 0),
(8, '2020-11-23', 14, 0, 0),
(9, '2020-11-23', 34, 0, 0),
(10, '2020-11-23', 12, 0, 0),
(11, '2020-11-23', 32, 0, 0),
(12, '2020-11-23', 11, 0, 0),
(13, '2020-11-23', 31, 0, 0),
(14, '2020-11-23', 8, 0, 0),
(15, '2020-11-23', 18, 0, 0),
(16, '2020-11-23', 28, 0, 0),
(17, '2020-11-23', 38, 0, 0),
(18, '2020-11-23', 10, 0, 0),
(19, '2020-11-23', 20, 0, 0),
(20, '2020-11-23', 30, 0, 0),
(21, '2020-11-23', 40, 0, 0),
(22, '2020-11-23', 9, 0, 0),
(23, '2020-11-23', 19, 0, 0),
(24, '2020-11-23', 29, 0, 0),
(25, '2020-11-23', 39, 0, 0),
(26, '2020-11-23', 7, 0, 0),
(27, '2020-11-23', 17, 0, 0),
(28, '2020-11-23', 27, 0, 0),
(29, '2020-11-23', 37, 0, 0),
(30, '2020-11-23', 6, 0, 0),
(31, '2020-11-23', 16, 0, 0),
(32, '2020-11-23', 26, 0, 0),
(33, '2020-11-23', 36, 0, 0),
(34, '2020-11-23', 3, 0, 0),
(35, '2020-11-23', 5, 0, 0),
(36, '2020-11-23', 4, 0, 0),
(37, '2020-11-23', 2, 0, 0),
(38, '2020-11-23', 1, 0, 0),
(39, '2020-11-23', 23, 0, 0),
(40, '2020-11-23', 25, 0, 0),
(41, '2020-11-23', 24, 0, 0),
(42, '2020-11-23', 22, 0, 0),
(43, '2020-11-23', 21, 0, 0),
(44, '2020-11-23', 45, 1, 0),
(45, '2020-11-23', 41, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(11) NOT NULL,
  `date_and_time_created` datetime DEFAULT NULL,
  `information` text DEFAULT NULL,
  `opened` tinyint(1) DEFAULT NULL,
  `rankings_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `id` int(11) NOT NULL,
  `date_found` date DEFAULT NULL,
  `advert_url` varchar(255) DEFAULT NULL,
  `target_webpages_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `rankings`
--

CREATE TABLE `rankings` (
  `id` int(11) NOT NULL,
  `rating_title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rankings`
--

INSERT INTO `rankings` (`id`, `rating_title`) VALUES
(1, 'Red Hot Lead'),
(2, 'Hot Lead'),
(3, 'Showing Some Promise'),
(4, 'Worth a shot'),
(5, 'Not looking too good'),
(6, 'Stone cold'),
(7, 'About as much chance as Scotland winning the world cup');

-- --------------------------------------------------------

--
-- Table structure for table `target_webpages`
--

CREATE TABLE `target_webpages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `target_url` text DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `picture` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `target_webpages`
--

INSERT INTO `target_webpages` (`id`, `title`, `target_url`, `notes`, `picture`) VALUES
(1, 'Indeed London PHP', 'https://www.indeed.co.uk/jobs?q=PHP&l=London&sr=directhire', '', ''),
(2, 'Indeed London NodeJS', 'https://www.indeed.co.uk/jobs?q=NodeJS&l=London&sr=directhire', '', ''),
(3, 'Indeed London Codeigniter', 'https://www.indeed.co.uk/jobs?q=Codeigniter&l=London&sr=directhire', '', ''),
(4, 'Indeed London JavaScript', 'https://www.indeed.co.uk/jobs?q=JavaScript&l=London&sr=directhire', '', ''),
(5, 'Indeed London Electron', 'https://www.indeed.co.uk/jobs?q=Electron&l=London&sr=directhire', '', ''),
(6, 'Indeed Glasgow PHP', 'https://www.indeed.co.uk/jobs?q=PHP&l=Glasgow&sr=directhire', '', ''),
(7, 'Indeed Glasgow NodeJS', 'https://www.indeed.co.uk/jobs?q=NodeJS&l=Glasgow&sr=directhire', '', ''),
(8, 'Indeed Glasgow Codeigniter', 'https://www.indeed.co.uk/jobs?q=Codeigniter&l=Glasgow&sr=directhire', '', ''),
(9, 'Indeed Glasgow JavaScript', 'https://www.indeed.co.uk/jobs?q=JavaScript&l=Glasgow&sr=directhire', '', ''),
(10, 'Indeed Glasgow Electron', 'https://www.indeed.co.uk/jobs?q=Electron&l=Glasgow&sr=directhire', '', ''),
(11, 'Indeed Edinburgh PHP', 'https://www.indeed.co.uk/jobs?q=PHP&l=Edinburgh&sr=directhire', '', ''),
(12, 'Indeed Edinburgh NodeJS', 'https://www.indeed.co.uk/jobs?q=NodeJS&l=Edinburgh&sr=directhire', '', ''),
(13, 'Indeed Edinburgh Codeigniter', 'https://www.indeed.co.uk/jobs?q=Codeigniter&l=Edinburgh&sr=directhire', '', 'logo_indeed.png'),
(14, 'Indeed Edinburgh JavaScript', 'https://www.indeed.co.uk/jobs?q=JavaScript&l=Edinburgh&sr=directhire', '', ''),
(15, 'Indeed Edinburgh Electron', 'https://www.indeed.co.uk/jobs?q=Electron&l=Edinburgh&sr=directhire', '', ''),
(16, 'Indeed Glasgow PHP', 'https://www.indeed.co.uk/jobs?q=PHP&l=Glasgow&sr=directhire', '', ''),
(17, 'Indeed Glasgow NodeJS', 'https://www.indeed.co.uk/jobs?q=NodeJS&l=Glasgow&sr=directhire', '', ''),
(18, 'Indeed Glasgow Codeigniter', 'https://www.indeed.co.uk/jobs?q=Codeigniter&l=Glasgow&sr=directhire', '', ''),
(19, 'Indeed Glasgow JavaScript', 'https://www.indeed.co.uk/jobs?q=JavaScript&l=Glasgow&sr=directhire', '', ''),
(20, 'Indeed Glasgow Electron', 'https://www.indeed.co.uk/jobs?q=Electron&l=Glasgow&sr=directhire', '', ''),
(21, 'Indeed Manchester PHP', 'https://www.indeed.co.uk/jobs?q=PHP&l=Manchester&sr=directhire', '', ''),
(22, 'Indeed Manchester NodeJS', 'https://www.indeed.co.uk/jobs?q=NodeJS&l=Manchester&sr=directhire', '', ''),
(23, 'Indeed Manchester Codeigniter', 'https://www.indeed.co.uk/jobs?q=Codeigniter&l=Manchester&sr=directhire', '', ''),
(24, 'Indeed Manchester JavaScript', 'https://www.indeed.co.uk/jobs?q=JavaScript&l=Manchester&sr=directhire', '', ''),
(25, 'Indeed Manchester Electron', 'https://www.indeed.co.uk/jobs?q=Electron&l=Manchester&sr=directhire', '', ''),
(26, 'Indeed Glasgow PHP', 'https://www.indeed.co.uk/jobs?q=PHP&l=Glasgow&sr=directhire', '', ''),
(27, 'Indeed Glasgow NodeJS', 'https://www.indeed.co.uk/jobs?q=NodeJS&l=Glasgow&sr=directhire', '', ''),
(28, 'Indeed Glasgow Codeigniter', 'https://www.indeed.co.uk/jobs?q=Codeigniter&l=Glasgow&sr=directhire', '', ''),
(29, 'Indeed Glasgow JavaScript', 'https://www.indeed.co.uk/jobs?q=JavaScript&l=Glasgow&sr=directhire', '', ''),
(30, 'Indeed Glasgow Electron', 'https://www.indeed.co.uk/jobs?q=Electron&l=Glasgow&sr=directhire', '', ''),
(31, 'Indeed Edinburgh PHP', 'https://www.indeed.co.uk/jobs?q=PHP&l=Edinburgh&sr=directhire', '', ''),
(32, 'Indeed Edinburgh NodeJS', 'https://www.indeed.co.uk/jobs?q=NodeJS&l=Edinburgh&sr=directhire', '', ''),
(33, 'Indeed Edinburgh Codeigniter', 'https://www.indeed.co.uk/jobs?q=Codeigniter&l=Edinburgh&sr=directhire', '', ''),
(34, 'Indeed Edinburgh JavaScript', 'https://www.indeed.co.uk/jobs?q=JavaScript&l=Edinburgh&sr=directhire', '', ''),
(35, 'Indeed Edinburgh Electron', 'https://www.indeed.co.uk/jobs?q=Electron&l=Edinburgh&sr=directhire', '', ''),
(36, 'Indeed Glasgow PHP', 'https://www.indeed.co.uk/jobs?q=PHP&l=Glasgow&sr=directhire', '', ''),
(37, 'Indeed Glasgow NodeJS', 'https://www.indeed.co.uk/jobs?q=NodeJS&l=Glasgow&sr=directhire', '', ''),
(38, 'Indeed Glasgow Codeigniter', 'https://www.indeed.co.uk/jobs?q=Codeigniter&l=Glasgow&sr=directhire', '', ''),
(39, 'Indeed Glasgow JavaScript', 'https://www.indeed.co.uk/jobs?q=JavaScript&l=Glasgow&sr=directhire', '', ''),
(40, 'Indeed Glasgow Electron', 'https://www.indeed.co.uk/jobs?q=Electron&l=Glasgow&sr=directhire', '', ''),
(41, 'S1Jobs PHP - no agencies', 'https://www.s1jobs.com/jobs/?keywords_required=PHP&salary_lo=0&salary_hi=600000&a_or_e=employer&facet_search=1&order_by=best-match&cb=9173509', '', 'logo_s1.png'),
(42, 'Gumtree PHP Nationwide', 'https://www.gumtree.com/search?search_category=it-jobs&search_location=glasgow&q=php&distance=1000', '', ''),
(43, 'Gumtree Nationwide Codeigniter', 'https://www.gumtree.com/search?search_category=it-jobs&search_location=glasgow&q=codeigniter&distance=1000', NULL, 'logo_gumtree.png'),
(44, 'Gumtree Nationwide JavaScript', 'https://www.gumtree.com/search?search_category=it-jobs&search_location=glasgow&q=javascript&distance=1000', NULL, ''),
(45, 'S1Jobs JavaScript - no agencies', 'https://www.s1jobs.com/jobs/?keywords_required=Javascript+Developer&salary_lo=0&salary_hi=600000&a_or_e=employer&facet_search=1&publicsector=publicsector&datesmart=4+weeks&onlyshowme=datesmart&cb=2524412', NULL, 'logo_s1.png');

-- --------------------------------------------------------

--
-- Table structure for table `trongate_administrators`
--

CREATE TABLE `trongate_administrators` (
  `id` int(11) NOT NULL,
  `username` varchar(65) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `trongate_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trongate_administrators`
--

INSERT INTO `trongate_administrators` (`id`, `username`, `password`, `trongate_user_id`) VALUES
(1, 'admin', '$2y$11$SoHZDvbfLSRHAi3WiKIBiu.tAoi/GCBBO4HRxVX1I3qQkq3wCWfXi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trongate_tokens`
--

CREATE TABLE `trongate_tokens` (
  `id` int(11) NOT NULL,
  `token` varchar(125) DEFAULT NULL,
  `user_id` int(11) DEFAULT 0,
  `expiry_date` int(11) DEFAULT NULL,
  `code` varchar(3) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trongate_tokens`
--

INSERT INTO `trongate_tokens` (`id`, `token`, `user_id`, `expiry_date`, `code`) VALUES
(18, 'nIqiBV7htmb8znNdHXottHj8gGFAgJ-i', 0, 1606094360, 'aaa'),
(19, '1uJ5LhGnjoPuKRTGkFj8gFGzd3wt58wx', 1, 1606167403, '0'),
(20, 'gYCirl7LNOSefB81vjviB53KETWsoE_b', 1, 1606100901, 'aaa');

-- --------------------------------------------------------

--
-- Table structure for table `trongate_users`
--

CREATE TABLE `trongate_users` (
  `id` int(11) NOT NULL,
  `code` varchar(32) DEFAULT NULL,
  `user_level_id` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trongate_users`
--

INSERT INTO `trongate_users` (`id`, `code`, `user_level_id`) VALUES
(1, '4kVjmgbG5cnWyN1kOmFPgpkXJ2MQ4ItF', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trongate_user_levels`
--

CREATE TABLE `trongate_user_levels` (
  `id` int(11) NOT NULL,
  `level_title` varchar(125) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trongate_user_levels`
--

INSERT INTO `trongate_user_levels` (`id`, `level_title`) VALUES
(1, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adverts`
--
ALTER TABLE `adverts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `associated_target_webpages_and_daily_checks`
--
ALTER TABLE `associated_target_webpages_and_daily_checks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `daily_checks`
--
ALTER TABLE `daily_checks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rankings`
--
ALTER TABLE `rankings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `target_webpages`
--
ALTER TABLE `target_webpages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trongate_administrators`
--
ALTER TABLE `trongate_administrators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trongate_tokens`
--
ALTER TABLE `trongate_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trongate_users`
--
ALTER TABLE `trongate_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trongate_user_levels`
--
ALTER TABLE `trongate_user_levels`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adverts`
--
ALTER TABLE `adverts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `associated_target_webpages_and_daily_checks`
--
ALTER TABLE `associated_target_webpages_and_daily_checks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `daily_checks`
--
ALTER TABLE `daily_checks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rankings`
--
ALTER TABLE `rankings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `target_webpages`
--
ALTER TABLE `target_webpages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `trongate_administrators`
--
ALTER TABLE `trongate_administrators`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `trongate_tokens`
--
ALTER TABLE `trongate_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `trongate_users`
--
ALTER TABLE `trongate_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `trongate_user_levels`
--
ALTER TABLE `trongate_user_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
