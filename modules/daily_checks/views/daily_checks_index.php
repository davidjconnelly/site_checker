<h1>Daily Checks <span class="smaller">(<?= $num_adverts_found_today ?> ads found today - <?= date('l jS F Y') ?>)</span></h1>
<?= validation_errors() ?>

<div class="target-pages-grid" bp="grid 1">
    <?php
    foreach($checks_to_do as $check_to_do) {

        $first_two = substr($check_to_do->title, 0, 2);

        switch ($first_two) {
            case 'In':
                $logo_path = 'target_webpages_pics/13/logo_indeed.png';
                break;
            case 'S1':
                $logo_path = 'target_webpages_pics/41/logo_s1.png';
                break;
            default:
                $logo_path = 'target_webpages_pics/43/logo_gumtree.png';
                break;
        }

        $background_color = 'red';
    
        if ($check_to_do->checked>0) {
            $background_color = 'yellow';
        }

        if ($check_to_do->num_adverts_found>0) {
            $background_color = 'lime';
        }
        ?>
        <div id="target-webpage-<?= $check_to_do->target_webpage_id ?>" style="background-color: <?= $background_color ?>">
            <div><img src="<?= BASE_URL.$logo_path ?>" class="logo"></div>
            <div><?= $check_to_do->title ?> (<?= $check_to_do->num_adverts_found ?>)</div>
        </div>
        <?php 
    }
    ?>
</div>

<style>

    .target-pages-grid > div {
        border: 1px black solid;
        padding: 4px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        text-align: center;
        font-size: 12px;
        font-weight: bold;
    }

    .logo {
        max-width: 70px;
        cursor: pointer;
    }

    .smaller {
        font-size: 0.5em;
    }
</style>

<script>
    var logos = document.getElementsByClassName("logo");
    for (var i = 0; i < logos.length; i++) {
        listenForClick(i);
    }

    function listenForClick(index) {

        logos[index].addEventListener("click", (ev) => {
            var parentDiv = ev.target.parentNode.parentNode;
            var parentDivId = parentDiv.id;
            var targetWebpageId = parentDivId.replace('target-webpage-', '');
            document.getElementById(parentDivId).style.backgroundColor = 'lime';

            //fetch the target URL
            var params = {
                targetWebpageId
            }

            var apiUrl = '<?= BASE_URL ?>target_webpages/fetch_url';
            const http = new XMLHttpRequest();
            http.open('POST', apiUrl);
            http.setRequestHeader('Content-type', 'application/json');
            http.send(JSON.stringify(params));
            http.onload = function() {
                // console.log(http.responseText);
                // alert(http.responseText);

                setTimeout(() => {
                    window.location.href = "<?= BASE_URL ?>adverts/check_for_adverts_init/" + targetWebpageId;
                }, 1000);

                window.open(http.responseText);
            }

        });
    }

</script>

