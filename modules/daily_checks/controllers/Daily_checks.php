<?php
class Daily_checks extends Trongate {

    function clear() {
        $tables_to_go[] = 'adverts';
        $tables_to_go[] = 'daily_checks';
        $tables_to_go[] = 'enquiries';
        $tables_to_go[] = 'comments';
        $tables_to_go[] = 'leads';

        foreach ($tables_to_go as $table) {
            $sql = 'truncate '.$table;
            $this->model->query($sql);
        }

        echo 'done<br>';
        echo anchor('daily_checks/index', 'Return to index');
    }

    function index() {
        $this->_init_daily_checks();
        
        $params['today'] = date('Y-m-d');
        $sql = 'SELECT
                    site_checker.target_webpages.title,
                    site_checker.daily_checks.id,
                    site_checker.daily_checks.date_checked,
                    site_checker.daily_checks.target_webpage_id,
                    site_checker.daily_checks.checked,
                    site_checker.daily_checks.num_adverts_found
                FROM
                    site_checker.daily_checks
                INNER JOIN site_checker.target_webpages ON site_checker.daily_checks.target_webpage_id = site_checker.target_webpages.id 
                WHERE daily_checks.date_checked = :today 
                ORDER by daily_checks.id';

        $data['checks_to_do'] = $this->model->query_bind($sql, $params, 'object');
        $data['num_adverts_found_today'] = $this->_get_num_adverts_found_today($data['checks_to_do']);
        $data['view_file'] = 'daily_checks_index';
        $this->template('admin', $data);
    }

    function _get_num_adverts_found_today($checks_to_do) {
        $num_adverts_found_today = 0;
        foreach($checks_to_do as $check_to_do) {
            $num_adverts_found_today = $num_adverts_found_today+$check_to_do->num_adverts_found;
        }

        return $num_adverts_found_today;
    }

    function _init_daily_checks() {
        $params['today'] = date('Y-m-d');
        $sql = ' select * from daily_checks where date_checked = :today order by id';
        $rows = $this->model->query_bind($sql, $params, 'object');

        $num_rows = count($rows);

        if ($num_rows<1) {
            
            //fetch all of the target sites
            $data['date_checked'] = $params['today'];
            $data['checked'] = 0;
            $data['num_adverts_found'] = 0;
            $target_webpages = $this->model->get('title', 'target_webpages');
            foreach($target_webpages as $target_webpage) {
                $data['target_webpage_id'] = $target_webpage->id;
                $this->model->insert($data, 'daily_checks');
            }

        }

    }

    function indexOLD2() {
        $this->_init_daily_checks();

        $sql = 'SELECT
                    daily_checks.id,
                    daily_checks.checked,
                    daily_checks.num_adverts_found,
                    target_webpages.id as webpage_id, 
                    target_webpages.title,
                    target_webpages.target_url 
                FROM
                    target_webpages
                INNER JOIN daily_checks ON target_webpages.id = daily_checks.target_webpage_id 
                WHERE daily_checks.date_checked = CURDATE() 
                ORDER BY target_webpages.title';
        $data['target_webpages'] = $this->model->query($sql, 'object');
        $data['view_file'] = 'daily_checks_index';
        $this->template('admin', $data);

    }

    function indexOLD() {
        $this->_init_daily_checks();

        //$data['target_webpages'] = $this->model->get('title', 'target_webpages');

        $sql = 'SELECT
                    daily_checks.checked,
                    daily_checks.num_adverts_found,
                    target_webpages.id, 
                    target_webpages.title,
                    target_webpages.target_url 
                FROM
                    target_webpages
                INNER JOIN daily_checks ON target_webpages.id = daily_checks.target_webpage_id 
                WHERE daily_checks.date_checked = CURDATE() 
                ORDER BY target_webpages.title';
        $data['target_webpages'] = $this->model->query($sql, 'object');
        $data['view_file'] = 'daily_checks_index';
        $this->template('admin', $data);
    }

    function _init_daily_checksOLD() {
        
        $registered_sites = [];
        //fetch all of the entries on daily_checks for today
        $site_checks = $this->model->get('id', 'daily_checks');

        $params['today'] = date('Y-m-d');
        $sql = ' select * from daily_checks where date_checked = :today order by id';
        $site_checks = $this->model->query_bind($sql, $params, 'object');

        foreach($site_checks as $site_check) {
            $registered_sites[] = $site_check->target_webpage_id;
        }

        //get an array of all of the target pages
        $all_target_pages = $this->model->get('id', 'target_webpages');
        $sites_to_register = [];
        foreach($all_target_pages as $target_page) {

            if (isset($target_page->id)) {

                if (!in_array($target_page->id, $registered_sites)) {
                    $sites_to_register[] = $target_page->id;
                }

            }

        }

        foreach($sites_to_register as $site_to_check_id) {
            $data['date_checked'] = date('Y-m-d');
            $data['target_webpage_id'] = $site_to_check_id;
            $data['checked'] = 0;
            $data['num_adverts_found'] = 0;
            $this->model->insert($data);
        }

    }

    function manage() {
        redirect('daily_checks/index');
        $this->module('security');
        $data['token'] = $this->security->_make_sure_allowed();
        $data['order_by'] = 'date_checked desc';

        //format the pagination
        $data['total_rows'] = $this->model->count('daily_checks');
        $data['record_name_plural'] = 'daily checks';

        $data['headline'] = 'Manage Daily Checks';
        $data['view_module'] = 'daily_checks';
        $data['view_file'] = 'manage';

        $this->template('admin', $data);
    }

    function show() {
        $this->module('security');
        $token = $this->security->_make_sure_allowed();

        $update_id = $this->url->segment(3);

        if ((!is_numeric($update_id)) && ($update_id != '')) {
            redirect('daily_checks/manage');
        }

        $data = $this->_get_data_from_db($update_id);
        $data['token'] = $token;

        if ($data == false) {
            redirect('daily_checks/manage');
        } else {
            $data['form_location'] = BASE_URL.'daily_checks/submit/'.$update_id;
            $data['update_id'] = $update_id;
            $data['headline'] = 'Daily Check Information';
            $data['date_checked'] = $this->_date_to_words($data['date_checked']);
            $data['view_file'] = 'show';
            $this->template('admin', $data);
        }
    }

    function create() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $update_id = $this->url->segment(3);
        $submit = $this->input('submit', true);

        if ((!is_numeric($update_id)) && ($update_id != '')) {
            redirect('daily_checks/manage');
        }

        //fetch the form data
        if (($submit == '') && ($update_id > 0)) {
            $data = $this->_get_data_from_db($update_id);
            $data['date_checked'] = date('m/d/Y', strtotime($data['date_checked']));
        } else {
            $data = $this->_get_data_from_post();
        }

        $data['headline'] = $this->_get_page_headline($update_id);

        if ($update_id > 0) {
            $data['cancel_url'] = BASE_URL.'daily_checks/show/'.$update_id;
            $data['btn_text'] = 'UPDATE DAILY CHECK DETAILS';
        } else {
            $data['cancel_url'] = BASE_URL.'daily_checks/manage';
            $data['btn_text'] = 'CREATE DAILY CHECK RECORD';
        }

        $additional_includes_top[] = 'https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css';
        $additional_includes_top[] = 'https://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.css';
        $additional_includes_top[] = 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"';
        $additional_includes_top[] = 'https://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.js';
        $additional_includes_top[] = BASE_URL.'admin_files/js/i18n/jquery-ui-timepicker-addon-i18n.min.js';
        $additional_includes_top[] = BASE_URL.'admin_files/js/jquery-ui-sliderAccess.js';
        $data['additional_includes_top'] = $additional_includes_top;

        $data['form_location'] = BASE_URL.'daily_checks/submit/'.$update_id;
        $data['update_id'] = $update_id;
        $data['view_file'] = 'create';
        $this->template('admin', $data);
    }

    function _get_page_headline($update_id) {
        //figure out what the page headline should be (on the daily_checks/create page)
        if (!is_numeric($update_id)) {
            $headline = 'Create New Daily Check Record';
        } else {
            $headline = 'Update Daily Check Details';
        }

        return $headline;
    }

    function submit() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $submit = $this->input('submit', true);

        if ($submit == 'Submit') {

            $this->validation_helper->set_rules('date_checked', 'Date Checked', 'required|valid_datepicker_us');

            $result = $this->validation_helper->run();

            if ($result == true) {

                $update_id = $this->url->segment(3);
                $data = $this->_get_data_from_post();

                //convert date picker into db friendly format
                $data['date_checked'] = date('Y-m-d', strtotime($data['date_checked']));

                if (is_numeric($update_id)) {
                    //update an existing record
                    $this->model->update($update_id, $data, 'daily_checks');
                    $flash_msg = 'The record was successfully updated';
                } else {
                    //insert the new record
                    $update_id = $this->model->insert($data, 'daily_checks');
                    $flash_msg = 'The record was successfully created';
                }

                set_flashdata($flash_msg);
                redirect('daily_checks/show/'.$update_id);

            } else {
                //form submission error
                $this->create();
            }

        }

    }

    function submit_delete() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $submit = $this->input('submit', true);

        if ($submit == 'Submit') {
            $update_id = $this->url->segment(3);

            if (!is_numeric($update_id)) {
                die();
            } else {
                $data['update_id'] = $update_id;

                //delete all of the comments associated with this record
                $sql = 'delete from comments where target_table = :module and update_id = :update_id';
                $data['module'] = $this->module;
                $this->model->query_bind($sql, $data);

                //delete the record
                $this->model->delete($update_id, $this->module);

                //set the flashdata
                $flash_msg = 'The record was successfully deleted';
                set_flashdata($flash_msg);

                //redirect to the manage page
                redirect('daily_checks/manage');
            }
        }
    }

    function _get_data_from_db($update_id) {
        $daily_checks = $this->model->get_where($update_id, 'daily_checks');

        if ($daily_checks == false) {
            $this->template('error_404');
            die();
        } else {
            $data['date_checked'] = $daily_checks->date_checked;
            return $data;
        }
    }

    function _get_data_from_post() {
        $data['date_checked'] = $this->input('date_checked', true);
        return $data;
    }

    function _date_to_words($date) {
        $date = date('l, F jS Y', strtotime($date));
        return $date;
    }

    function _prep_output($output) {
        $output['body'] = json_decode($output['body']);
        foreach($output['body'] as $key => $value) {
            $output['body'][$key] ->date_checked = $this->_date_to_words($value->date_checked);
        }

        $output['body'] = json_encode($output['body']);

        return $output;
    }

}