<?php
class Target_webpages extends Trongate {

    function _calculate_city($target_url) {

        $city = 'London';

        $target_str1 = 'lasgow';
        $str_pos1 = strpos($target_url, $target_str1);

        if (is_numeric($str_pos1)) {
            $city = 'Glasgow';
        } else {

            $target_str2 = 'dinburgh';
            $str_pos2 = strpos($target_url, $target_str2);            

            if (is_numeric($str_pos2)) {
                $city = 'Edinburgh';
            }
            
        }

        return $city;
    }

    function fix() {
        $rows = $this->model->get("id");
        $start = 'https://www.indeed.co.uk/jobs?q=PHP&l=';
        $end = '&';

        foreach($rows as $row) {

            $city = $this->_calculate_city($row->target_url);
            if ($city !== 'London') {
                $data['title'] = str_replace('London', $city, $row->title);
                $this->model->update($row->id, $data);
            }
            
            echo 'done';
        }
    }

    function _extract_content($string, $start, $end) {
        $pos = stripos($string, $start);
        $str = substr($string, $pos);
        $str_two = substr($str, strlen($start));
        $second_pos = stripos($str_two, $end);
        $str_three = substr($str_two, 0, $second_pos);
        $content = trim($str_three); // remove whitespaces
        return $content;
    }

    function fetch_url() {

        $post = file_get_contents('php://input');
        $posted_data = json_decode($post, true);

        if (isset($posted_data)) {
            $update_id = $posted_data['targetWebpageId'];
        } else {
            $update_id = 43;
        }
        
        $webpage_obj = $this->model->get_one_where('id', $update_id, 'target_webpages');
        $target_url = $webpage_obj->target_url;
        $this->_set_to_checked($webpage_obj->id);

        echo $target_url;
    }

    function _set_to_checked($target_webpage_id) {

        $params['target_webpage_id'] = $target_webpage_id;
        $sql = 'select * from daily_checks where date_checked = CURDATE() and target_webpage_id = :target_webpage_id';
        $rows = $this->model->query_bind($sql, $params, 'object');
        $num_rows = count($rows);

        if ($num_rows>0) {
            $data['target_webpage_id'] = $params['target_webpage_id'];
            $data['checked'] = 1;
            $update_id = $rows[0]->id;
            $this->model->update($update_id, $data, 'daily_checks');
        }

    }

    function _init_picture_settings() { 
        $picture_settings['targetModule'] = 'target_webpages';
        $picture_settings['maxFileSize'] = 2000;
        $picture_settings['maxWidth'] = 1200;
        $picture_settings['maxHeight'] = 1200;
        $picture_settings['resizedMaxWidth'] = 450;
        $picture_settings['resizedMaxHeight'] = 450;
        $picture_settings['destination'] = 'target_webpages_pics';
        $picture_settings['targetColumnName'] = 'picture';
        $picture_settings['thumbnailDir'] = 'target_webpages_pics_thumbnails';
        $picture_settings['thumbnailMaxWidth'] = 120;
        $picture_settings['thumbnailMaxHeight'] = 120;
        return $picture_settings;
    }

    function manage() {
        $this->module('security');
        $data['token'] = $this->security->_make_sure_allowed();
        $data['order_by'] = 'title';

        //format the pagination
        $data['total_rows'] = $this->model->count('target_webpages');
        $data['record_name_plural'] = 'target webpages';

        $data['headline'] = 'Manage Target Webpages';
        $data['view_module'] = 'target_webpages';
        $data['view_file'] = 'manage';

        $this->template('admin', $data);
    }

    function show() {
        $this->module('security');
        $token = $this->security->_make_sure_allowed();

        $update_id = $this->url->segment(3);

        if ((!is_numeric($update_id)) && ($update_id != '')) {
            redirect('target_webpages/manage');
        }

        $data = $this->_get_data_from_db($update_id);
        $data['token'] = $token;

        if ($data == false) {
            redirect('target_webpages/manage');
        } else {
            $data['form_location'] = BASE_URL.'target_webpages/submit/'.$update_id;
            $data['update_id'] = $update_id;
            $data['headline'] = 'Target Webpage Information';
            $data['view_file'] = 'show';
            $this->template('admin', $data);
        }
    }

    function create() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $update_id = $this->url->segment(3);
        $submit = $this->input('submit', true);

        if ((!is_numeric($update_id)) && ($update_id != '')) {
            redirect('target_webpages/manage');
        }

        //fetch the form data
        if (($submit == '') && ($update_id > 0)) {
            $data = $this->_get_data_from_db($update_id);
        } else {
            $data = $this->_get_data_from_post();
        }

        $data['headline'] = $this->_get_page_headline($update_id);

        if ($update_id > 0) {
            $data['cancel_url'] = BASE_URL.'target_webpages/show/'.$update_id;
            $data['btn_text'] = 'UPDATE TARGET WEBPAGE DETAILS';
        } else {
            $data['cancel_url'] = BASE_URL.'target_webpages/manage';
            $data['btn_text'] = 'CREATE TARGET WEBPAGE RECORD';
        }

        $additional_includes_top[] = 'https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css';
        $additional_includes_top[] = 'https://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.css';
        $additional_includes_top[] = 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"';
        $additional_includes_top[] = 'https://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.js';
        $additional_includes_top[] = BASE_URL.'admin_files/js/i18n/jquery-ui-timepicker-addon-i18n.min.js';
        $additional_includes_top[] = BASE_URL.'admin_files/js/jquery-ui-sliderAccess.js';
        $data['additional_includes_top'] = $additional_includes_top;

        $data['form_location'] = BASE_URL.'target_webpages/submit/'.$update_id;
        $data['update_id'] = $update_id;
        $data['view_file'] = 'create';
        $this->template('admin', $data);
    }

    function _get_page_headline($update_id) {
        //figure out what the page headline should be (on the target_webpages/create page)
        if (!is_numeric($update_id)) {
            $headline = 'Create New Target Webpage Record';
        } else {
            $headline = 'Update Target Webpage Details';
        }

        return $headline;
    }

    function submit() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $submit = $this->input('submit', true);

        if ($submit == 'Submit') {

            $this->validation_helper->set_rules('title', 'Title', 'required|min_length[2]|max_length[255]');
            $this->validation_helper->set_rules('target_url', 'Target URL', 'required|min_length[2]|max_length[255]');
            $this->validation_helper->set_rules('notes', 'Notes', 'min_length[2]');

            $result = $this->validation_helper->run();

            if ($result == true) {

                $update_id = $this->url->segment(3);
                $data = $this->_get_data_from_post();
                if (is_numeric($update_id)) {
                    //update an existing record
                    $this->model->update($update_id, $data, 'target_webpages');
                    $flash_msg = 'The record was successfully updated';
                } else {
                    //insert the new record
                    $update_id = $this->model->insert($data, 'target_webpages');
                    $flash_msg = 'The record was successfully created';
                }

                set_flashdata($flash_msg);
                redirect('target_webpages/show/'.$update_id);

            } else {
                //form submission error
                $this->create();
            }

        }

    }

    function submit_delete() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $submit = $this->input('submit', true);

        if ($submit == 'Submit') {
            $update_id = $this->url->segment(3);

            if (!is_numeric($update_id)) {
                die();
            } else {
                $data['update_id'] = $update_id;

                //delete all of the comments associated with this record
                $sql = 'delete from comments where target_table = :module and update_id = :update_id';
                $data['module'] = $this->module;
                $this->model->query_bind($sql, $data);

                //delete the record
                $this->model->delete($update_id, $this->module);

                //set the flashdata
                $flash_msg = 'The record was successfully deleted';
                set_flashdata($flash_msg);

                //redirect to the manage page
                redirect('target_webpages/manage');
            }
        }
    }

    function _get_data_from_db($update_id) {
        $target_webpages = $this->model->get_where($update_id, 'target_webpages');

        if ($target_webpages == false) {
            $this->template('error_404');
            die();
        } else {
            $data['title'] = $target_webpages->title;
            $data['target_url'] = $target_webpages->target_url;
            $data['notes'] = $target_webpages->notes;
            return $data;
        }
    }

    function _get_data_from_post() {
        $data['title'] = $this->input('title', true);
        $data['target_url'] = $this->input('target_url', true);
        $data['notes'] = $this->input('notes', true);
        return $data;
    }

}