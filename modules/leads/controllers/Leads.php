<?php
class Leads extends Trongate {

    function manage() {
        $this->module('security');
        $data['token'] = $this->security->_make_sure_allowed();
        $data['order_by'] = 'date_found desc';

        //format the pagination
        $data['total_rows'] = $this->model->count('leads');
        $data['record_name_plural'] = 'leads';

        $data['headline'] = 'Manage Leads';
        $data['view_module'] = 'leads';
        $data['view_file'] = 'manage';

        $this->template('admin', $data);
    }

    function show() {
        $this->module('security');
        $token = $this->security->_make_sure_allowed();

        $update_id = $this->url->segment(3);

        if ((!is_numeric($update_id)) && ($update_id != '')) {
            redirect('leads/manage');
        }

        $data = $this->_get_data_from_db($update_id);
        $data['token'] = $token;

        if ($data == false) {
            redirect('leads/manage');
        } else {
            $data['form_location'] = BASE_URL.'leads/submit/'.$update_id;
            $data['update_id'] = $update_id;
            $data['headline'] = 'Lead Information';
            $data['date_found'] = $this->_date_to_words($data['date_found']);
            $data['view_file'] = 'show';
            $this->template('admin', $data);
        }
    }

    function _get_target_webpages_options($selected_key) {

        if ($selected_key == '') {
            $options[''] = 'Select...';
        }
        
        $sql = "select * from target_webpages order by title";
        $rows = $this->model->query($sql, 'object');

        foreach ($rows as $row) {
            $row_desc = $row->title;;
            $options[$row->id] = $row_desc;
        }

        if ($selected_key>0) {
            $row_label = $options[$selected_key];
            $options[0] = strtoupper('*** Disassociate with '.$row_label.' ***');
        }

        return $options;
    }

    function create() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $update_id = $this->url->segment(3);
        $submit = $this->input('submit', true);

        if ((!is_numeric($update_id)) && ($update_id != '')) {
            redirect('leads/manage');
        }

        //fetch the form data
        if (($submit == '') && ($update_id > 0)) {
            $data = $this->_get_data_from_db($update_id);
            $data['date_found'] = date('m/d/Y', strtotime($data['date_found']));
        } else {
            $data = $this->_get_data_from_post();
        }

        if ($data['target_webpages_id'] == 0) {
            $data['target_webpages_id'] = '';
        }

        $data['target_webpages_options'] = $this->_get_target_webpages_options($data['target_webpages_id']);
        $data['headline'] = $this->_get_page_headline($update_id);

        if ($update_id > 0) {
            $data['cancel_url'] = BASE_URL.'leads/show/'.$update_id;
            $data['btn_text'] = 'UPDATE LEAD DETAILS';
        } else {
            $data['cancel_url'] = BASE_URL.'leads/manage';
            $data['btn_text'] = 'CREATE LEAD RECORD';
        }

        $additional_includes_top[] = 'https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css';
        $additional_includes_top[] = 'https://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.css';
        $additional_includes_top[] = 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"';
        $additional_includes_top[] = 'https://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.js';
        $additional_includes_top[] = BASE_URL.'admin_files/js/i18n/jquery-ui-timepicker-addon-i18n.min.js';
        $additional_includes_top[] = BASE_URL.'admin_files/js/jquery-ui-sliderAccess.js';
        $data['additional_includes_top'] = $additional_includes_top;

        $data['form_location'] = BASE_URL.'leads/submit/'.$update_id;
        $data['update_id'] = $update_id;
        $data['view_file'] = 'create';
        $this->template('admin', $data);
    }

    function _get_page_headline($update_id) {
        //figure out what the page headline should be (on the leads/create page)
        if (!is_numeric($update_id)) {
            $headline = 'Create New Lead Record';
        } else {
            $headline = 'Update Lead Details';
        }

        return $headline;
    }

    function submit() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $submit = $this->input('submit', true);

        if ($submit == 'Submit') {

            $this->validation_helper->set_rules('date_found', 'Date Found', 'required|valid_datepicker_us');
            $this->validation_helper->set_rules('advert_url', 'Advert URL', 'required|min_length[2]|max_length[255]');

            $result = $this->validation_helper->run();

            if ($result == true) {

                $update_id = $this->url->segment(3);
                $data = $this->_get_data_from_post();

                //convert date picker into db friendly format
                $data['date_found'] = date('Y-m-d', strtotime($data['date_found']));

                if (is_numeric($update_id)) {
                    //update an existing record
                    $this->model->update($update_id, $data, 'leads');
                    $flash_msg = 'The record was successfully updated';
                } else {
                    //insert the new record
                    $update_id = $this->model->insert($data, 'leads');
                    $flash_msg = 'The record was successfully created';
                }

                set_flashdata($flash_msg);
                redirect('leads/show/'.$update_id);

            } else {
                //form submission error
                $this->create();
            }

        }

    }

    function submit_delete() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $submit = $this->input('submit', true);

        if ($submit == 'Submit') {
            $update_id = $this->url->segment(3);

            if (!is_numeric($update_id)) {
                die();
            } else {
                $data['update_id'] = $update_id;

                //delete all of the comments associated with this record
                $sql = 'delete from comments where target_table = :module and update_id = :update_id';
                $data['module'] = $this->module;
                $this->model->query_bind($sql, $data);

                //delete the record
                $this->model->delete($update_id, $this->module);

                //set the flashdata
                $flash_msg = 'The record was successfully deleted';
                set_flashdata($flash_msg);

                //redirect to the manage page
                redirect('leads/manage');
            }
        }
    }

    function _get_data_from_db($update_id) {
        $leads = $this->model->get_where($update_id, 'leads');

        if ($leads == false) {
            $this->template('error_404');
            die();
        } else {
            $data['date_found'] = $leads->date_found;
            $data['advert_url'] = $leads->advert_url;
            $data['target_webpages_id'] = $leads->target_webpages_id;
            return $data;
        }
    }

    function _get_data_from_post() {
        $data['date_found'] = $this->input('date_found', true);
        $data['advert_url'] = $this->input('advert_url', true);
        $data['target_webpages_id'] = $this->input('target_webpages_id', true);
        return $data;
    }

    function _date_to_words($date) {
        $date = date('l, F jS Y', strtotime($date));
        return $date;
    }

    function _prep_output($output) {
        $output['body'] = json_decode($output['body']);
        foreach($output['body'] as $key => $value) {
            $output['body'][$key] ->date_found = $this->_date_to_words($value->date_found);
        }

        $output['body'] = json_encode($output['body']);

        return $output;
    }

}