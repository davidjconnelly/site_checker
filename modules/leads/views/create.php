<p><h1><?= $headline ?></h1>
<?= validation_errors() ?>
<div class="w3-card-4">
    <div class="w3-container primary">
        <h4>Lead Details</h4>
    </div>
    <form class="w3-container" action="<?= $form_location ?>" method="post">

        <p>
            <label class="w3-text-dark-grey"><b>Date Found</b></label>
            <input type="text" name="date_found" value="<?= $date_found ?>" class="w3-input w3-border w3-sand datepicker" placeholder="Select Date Found">
        </p>
        <p>
            <label class="w3-text-dark-grey"><b>Advert URL</b></label>
            <input type="text" name="advert_url" value="<?= $advert_url ?>" class="w3-input w3-border w3-sand" placeholder="Enter Advert URL">
        </p>
        <p>
            <label class="w3-text-dark-grey"><b>Target Webpages ID</b></label>
            <?php
            $attributes['class'] = 'w3-select w3-border w3-sand';
            echo form_dropdown('target_webpages_id', $target_webpages_options, $target_webpages_id, $attributes);
            ?>
        </p>
        <p> 
            <?php 
            $attributes['class'] = 'w3-button w3-white w3-border';
            echo anchor($cancel_url, 'CANCEL', $attributes);
            ?> 
            <button type="submit" name="submit" value="Submit" class="w3-button w3-medium primary"><?= $btn_text ?></button>
        </p>
    </form>
</div>

<script>
$('.datepicker').datepicker();
$('.datetimepicker').datetimepicker({
    separator: ' at '
});
$('.timepicker').timepicker();
</script>