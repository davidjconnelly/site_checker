<h1>Results For <?= $target_webpage_title ?></h1>

<p>
    <a href="<?= BASE_URL ?>daily_checks/index"><button type="button" class="w3-button w3-white w3-border"><i class="fa fa-arrow-left"></i> Go Back</button></a>
    <button onclick="document.getElementById('create-advert-modal').style.display='block'" class="w3-button w3-white w3-border"><i class="fa fa-star"></i> FOUND ADVERT</button> 
</p>

<div id="create-advert-modal" class="w3-modal" style="padding-top: 7em;">
    <div class="w3-modal-content w3-animate-top w3-card-4" style="width: 30%;">
        <header class="w3-container primary w3-text-white">
            <h4><i class="fa fa-star"></i> FOUND ADVERT</h4>
        </header>
        <div class="w3-container">
            <p>Advert URL:
                <textarea name="advert" id="new-advert" class="w3-input w3-border w3-sand" placeholder="Enter advert URL here..." ></textarea>
            </p>
            <p>Optional Additional Text:
                <textarea name="notes" id="notes" class="w3-input w3-border w3-sand" placeholder="Enter additional notes here..." ></textarea>
            </p>
            <p class="w3-right modal-btns">
                <button onclick="document.getElementById('create-advert-modal').style.display='none'" type="button" name="submit" value="Submit" class="w3-button w3-small 3-white w3-border">CANCEL</button> 
                <button onclick="submitNewAdvert()" type="button" name="submit" value="Submit" class="w3-button w3-small primary">ADD ADVERT</button>
            </p>
        </div>
    </div>
</div>

<hr>
<div id="results"></div>

<script>
function submitNewAdvert() {
    var advert_url = document.getElementById("new-advert").value;
    advert_url = advert_url.trim();

    var notes = document.getElementById("notes").value;
    notes = notes.trim();

    if (advert_url == "") {
        return;
    } else {

        document.getElementById("create-advert-modal").style.display='none';
        document.getElementById("new-advert").value = '';
        document.getElementById("notes").value = '';

        const params = {
            advert_url,
            notes,
            target_webpage_id: <?= $target_webpage_id ?>
        }

        var target_url = '<?= BASE_URL ?>api/create/adverts';
        const http = new XMLHttpRequest()
        http.open('POST', target_url)
        http.setRequestHeader('Content-type', 'application/json')
        http.send(JSON.stringify(params)) 
        http.onload = function() {

            if (http.status == 422) {
                alert("already on database!");
            } else {
                populateAdvertsFound();
            }

        }

    }

}    

function populateAdvertsFound() {

    var params = {
        date_found: '<?= date('Y-m-d') ?>',
        target_webpage_id: <?= $target_webpage_id ?>
    }

    var apiUrl = '<?= BASE_URL ?>api/get/adverts';
    const http = new XMLHttpRequest();
    http.open('POST', apiUrl);
    http.setRequestHeader('Content-type', 'application/json');
    http.send(JSON.stringify(params));
    http.onload = function() {
        console.log(http.responseText);
        var results = JSON.parse(http.responseText);
        drawResults(results)
    }

    //get a number representing number of adverts found today

    //draw a list of URLs of adverts found

    //WHEN an advert is inserted > (1) create a new lead, (2) update_num_adverts found on daily_checks
}

function drawResults(results) {

    if (results.length>0) {

        var resultsHtml = '<p>Advert(s) Found:</p>';
        resultsHtml+= '<ol>';

        for (var i = 0; i < results.length; i++) {
            resultsHtml+= '<li><a href="' + results[i]["advert_url"] + '" target="_blank">' + results[i]["advert_url"] + '</a></li>';
        }

        resultsHtml+= '</ol>';

    } else {
        var resultsHtml = '<p>No adverts found so far.</p>';
    }
    document.getElementById("results").innerHTML = resultsHtml;
}

populateAdvertsFound();
</script>
