<h1><?= $headline ?></h1>
<?= validation_errors() ?>
<div class="w3-card-4">
    <div class="w3-container primary">
        <h4>Advert Details</h4>
    </div>
    <form class="w3-container" action="<?= $form_location ?>" method="post">

        <p>
            <label class="w3-text-dark-grey"><b>Date Found</b></label>
            <input type="text" name="date_found" value="<?= $date_found ?>" class="w3-input w3-border w3-sand" placeholder="Enter Date Found">
        </p>
        <p>
            <label class="w3-text-dark-grey"><b>Advert URL</b></label>
            <input type="text" name="advert_url" value="<?= $advert_url ?>" class="w3-input w3-border w3-sand" placeholder="Enter Advert URL">
        </p>
        <p>
            <label class="w3-text-dark-grey"><b>Notes</b></label>
            <textarea name="notes" class="w3-input w3-border w3-sand" placeholder="Enter Notes here..."><?= $notes ?></textarea>
        </p>
        <p>
            <label class="w3-text-dark-grey"><b>Target Webpage ID</b> <span class="w3-text-green">(optional)</span></label>
            <input type="text" name="target_webpage_id" value="<?= $target_webpage_id ?>" class="w3-input w3-border w3-sand" placeholder="Enter Target Webpage ID">
        </p>
        <p> 
            <?php 
            $attributes['class'] = 'w3-button w3-white w3-border';
            echo anchor($cancel_url, 'CANCEL', $attributes);
            ?> 
            <button type="submit" name="submit" value="Submit" class="w3-button w3-medium primary"><?= $btn_text ?></button>
        </p>
    </form>
</div>

<script>
$('.datepicker').datepicker();
$('.datetimepicker').datetimepicker({
    separator: ' at '
});
$('.timepicker').timepicker();
</script>