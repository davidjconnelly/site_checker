<?php
class Adverts extends Trongate {

    function pre_insert($input) {
        $posted_data = $input['params'];

        $params['advert_url'] = $posted_data['advert_url'];
        $params['notes'] = $posted_data['notes'];
        $sql = 'select * from adverts where advert_url=:advert_url and notes=:notes';
        $rows = $this->model->query_bind($sql, $params, 'object');
        $num_rows = count($rows);

        if ($num_rows == 0) {
            $data['date_found'] = date('Y-m-d');
            $data['notes'] = $posted_data['notes'];
            $data['target_webpage_id'] = $posted_data['target_webpage_id'];
            $data['advert_url'] = $posted_data['advert_url'];
            $input['params'] = $data;
            return $input;

        } else {

            http_response_code(422);
            echo 'advert already on db';
            die();

        }


    }

    function post_insert($output) {
        $body = json_decode($output['body']);
        $new_record_id = $body->id;
        $target_webpage_id = $body->target_webpage_id;

        //update num_adverts found on daily_checks
        $params['target_webpage_id'] = $target_webpage_id;
        $params['date_checked'] = date('Y-m-d');

        $sql = 'select * from daily_checks where date_checked=:date_checked and 
                target_webpage_id=:target_webpage_id';
        $rows = $this->model->query_bind($sql, $params, 'object');
        if (isset($rows[0])) {
            $target_record = $rows[0];
            $update_id = $target_record->id;
            $data['num_adverts_found'] = $target_record->num_adverts_found+1;
            
            $this->model->update($update_id, $data, 'daily_checks');

            $this->_generate_new_enquiry($body);
            echo 'great success';
            die();
        }

    }

    function _generate_new_enquiry($body) {

        $information = 'An advert was found on WEBSITE_TITLE website.  The URL for the advert is AD_URL';

        $target_webpage_id = $body->target_webpage_id;
        $website_obj = $this->model->get_where($target_webpage_id, 'target_webpages');
        $website_title = $website_obj->title;

        $information = str_replace('WEBSITE_TITLE', $website_title, $information);

        $advert_url = '<a href="'.$body->advert_url.'" target="_blank">'.$body->advert_url.'</a>.';
        $information = str_replace('AD_URL', $advert_url, $information);

        $notes = $body->notes;

        if ($notes !== '') {
            $information.='<br><br>'.$notes;
        }

        $data['date_and_time_created'] = date('Y-m-d H:i:s');
        $data['information'] = $information;
        $data['opened'] = 0;
        $data['rankings_id'] = 0;
        $this->model->insert($data, 'enquiries');

    }

    function _get_target_webpage_id($daily_check_id) {
        $daily_check_obj = $this->model->get_where($daily_check_id, 'daily_checks');
        $target_webpage_id = $daily_check_obj->target_webpage_id;
        return $target_webpage_id;
    }

    function check_for_adverts_init() {
        $params['target_webpage_id'] = $this->url->segment(3);
        $params['today'] = date('Y-m-d');
        $sql = 'select * from daily_checks where date_checked = :today and target_webpage_id = :target_webpage_id';
        $rows = $this->model->query_bind($sql, $params, 'object');
        $target_row = $rows[0];

        $id = $target_row->id;
        redirect('adverts/check_for_adverts/'.$id);
    }

    function check_for_adverts() {
        $data['daily_check_id'] = $this->url->segment(3);
        $this->_make_sure_checked($data['daily_check_id']);
        $data['target_webpage_id'] = $this->_get_target_webpage_id($data['daily_check_id']);
        $data['target_webpage_title'] = $this->_get_target_webpage_title($data['target_webpage_id']);
        $data['view_file'] = 'check_for_adverts';
        $this->template('admin', $data);
    }

    function _make_sure_checked($update_id) {
        $data['checked'] = 1;
        $this->model->update($update_id, $data, 'daily_checks');
    }

    function _get_target_webpage_title($id) {
        $webpage_obj = $this->model->get_one_where('id', $id, 'target_webpages');
        $webpage_title = $webpage_obj->title;
        return $webpage_title;
    }

    function manage() {
        $this->module('security');
        $data['token'] = $this->security->_make_sure_allowed();
        $data['order_by'] = 'date_found desc';

        //format the pagination
        $data['total_rows'] = $this->model->count('adverts');
        $data['record_name_plural'] = 'adverts';

        $data['headline'] = 'Manage Adverts';
        $data['view_module'] = 'adverts';
        $data['view_file'] = 'manage';

        $this->template('admin', $data);
    }

    function show() {
        $this->module('security');
        $token = $this->security->_make_sure_allowed();

        $update_id = $this->url->segment(3);

        if ((!is_numeric($update_id)) && ($update_id != '')) {
            redirect('adverts/manage');
        }

        $data = $this->_get_data_from_db($update_id);
        $data['token'] = $token;

        if ($data == false) {
            redirect('adverts/manage');
        } else {
            $data['form_location'] = BASE_URL.'adverts/submit/'.$update_id;
            $data['update_id'] = $update_id;
            $data['headline'] = 'Advert Information';
            $data['view_file'] = 'show';
            $this->template('admin', $data);
        }
    }

    function create() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $update_id = $this->url->segment(3);
        $submit = $this->input('submit', true);

        if ((!is_numeric($update_id)) && ($update_id != '')) {
            redirect('adverts/manage');
        }

        //fetch the form data
        if (($submit == '') && ($update_id > 0)) {
            $data = $this->_get_data_from_db($update_id);
        } else {
            $data = $this->_get_data_from_post();
        }

        $data['headline'] = $this->_get_page_headline($update_id);

        if ($update_id > 0) {
            $data['cancel_url'] = BASE_URL.'adverts/show/'.$update_id;
            $data['btn_text'] = 'UPDATE ADVERT DETAILS';
        } else {
            $data['cancel_url'] = BASE_URL.'adverts/manage';
            $data['btn_text'] = 'CREATE ADVERT RECORD';
        }

        $additional_includes_top[] = 'https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css';
        $additional_includes_top[] = 'https://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.css';
        $additional_includes_top[] = 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"';
        $additional_includes_top[] = 'https://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.js';
        $additional_includes_top[] = BASE_URL.'admin_files/js/i18n/jquery-ui-timepicker-addon-i18n.min.js';
        $additional_includes_top[] = BASE_URL.'admin_files/js/jquery-ui-sliderAccess.js';
        $data['additional_includes_top'] = $additional_includes_top;

        $data['form_location'] = BASE_URL.'adverts/submit/'.$update_id;
        $data['update_id'] = $update_id;
        $data['view_file'] = 'create';
        $this->template('admin', $data);
    }

    function _get_page_headline($update_id) {
        //figure out what the page headline should be (on the adverts/create page)
        if (!is_numeric($update_id)) {
            $headline = 'Create New Advert Record';
        } else {
            $headline = 'Update Advert Details';
        }

        return $headline;
    }

    function submit() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $submit = $this->input('submit', true);

        if ($submit == 'Submit') {

            $this->validation_helper->set_rules('date_found', 'Date Found', 'required|max_length[11]|numeric|greater_than[0]|integer');
            $this->validation_helper->set_rules('advert_url', 'Advert URL', 'required|min_length[2]|max_length[255]');
            $this->validation_helper->set_rules('notes', 'Notes', 'required|min_length[2]');
            $this->validation_helper->set_rules('target_webpage_id', 'Target Webpage ID', 'integer');

            $result = $this->validation_helper->run();

            if ($result == true) {

                $update_id = $this->url->segment(3);
                $data = $this->_get_data_from_post();
                if (is_numeric($update_id)) {
                    //update an existing record
                    $this->model->update($update_id, $data, 'adverts');
                    $flash_msg = 'The record was successfully updated';
                } else {
                    //insert the new record
                    $update_id = $this->model->insert($data, 'adverts');
                    $flash_msg = 'The record was successfully created';
                }

                set_flashdata($flash_msg);
                redirect('adverts/show/'.$update_id);

            } else {
                //form submission error
                $this->create();
            }

        }

    }

    function submit_delete() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $submit = $this->input('submit', true);

        if ($submit == 'Submit') {
            $update_id = $this->url->segment(3);

            if (!is_numeric($update_id)) {
                die();
            } else {
                $data['update_id'] = $update_id;

                //delete all of the comments associated with this record
                $sql = 'delete from comments where target_table = :module and update_id = :update_id';
                $data['module'] = $this->module;
                $this->model->query_bind($sql, $data);

                //delete the record
                $this->model->delete($update_id, $this->module);

                //set the flashdata
                $flash_msg = 'The record was successfully deleted';
                set_flashdata($flash_msg);

                //redirect to the manage page
                redirect('adverts/manage');
            }
        }
    }

    function _get_data_from_db($update_id) {
        $adverts = $this->model->get_where($update_id, 'adverts');

        if ($adverts == false) {
            $this->template('error_404');
            die();
        } else {
            $data['date_found'] = $adverts->date_found;
            $data['advert_url'] = $adverts->advert_url;
            $data['notes'] = $adverts->notes;
            $data['target_webpage_id'] = $adverts->target_webpage_id;
            return $data;
        }
    }

    function _get_data_from_post() {
        $data['date_found'] = $this->input('date_found', true);
        $data['advert_url'] = $this->input('advert_url', true);
        $data['notes'] = $this->input('notes', true);
        $data['target_webpage_id'] = $this->input('target_webpage_id', true);
        return $data;
    }

}