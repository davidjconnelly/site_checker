<?php
class Enquiries extends Trongate {

    function open_enquiry($update_id) {
        $data['opened'] = 1;
        $this->model->update($update_id, $data);
        $target_url = str_replace('/open_enquiry/', '/show/', current_url());
        redirect($target_url);
    }

    function _get_rankings() {
        $rankings[0] = 'not ranked';
        $rows = $this->model->get('id', 'rankings');
        foreach ($rows as $row) {
            $rankings[$row->id] = $row->rating_title;
        }

        return $rankings;
    }

    function manage() {
        $this->module('security');
        $this->security->_make_sure_allowed();
        $data['rankings'] = $this->_get_rankings();
        $data['rows'] = $this->_fetch_rows();
        $data['limit'] = $this->_get_limit();
        $data['set_limit_url'] = BASE_URL.'enquiries/set_limit/';
        $data['total_rows'] = $this->_fetch_rows(true);
        $data['include_showing_statement'] = true;
        $data['record_name_plural'] = 'enquiries';

        //display 'view all' btn if search submitted
        if ($this->url->segment(4) !== '') {
            $data['include_view_all_btn'] = true;
        }

        $data['headline'] = 'Manage Enquiries';
        $data['view_module'] = 'enquiries';
        $data['view_file'] = 'manage';
        $this->template('admin', $data);
    }

    function submit_search() {

        $this->module('security');
        $this->security->_make_sure_allowed();

        $submit = $this->input('submit');

        if ($submit == 'Search') {
            $search_phrase = urlencode(ltrim(trim($this->input('search_phrase'))));
            $target_url = BASE_URL.'enquiries/manage/1/'.$search_phrase;
            redirect($target_url);
        }

    }

    function not_found() {
        $this->module('security');
        $this->security->_make_sure_allowed();
        $data['try_again_url'] = BASE_URL.'enquiries/manage';
        $data['view_module'] = 'enquiries';
        $data['view_file'] = 'not_found';
        $data['search_phrase'] = urldecode($this->url->segment(3));
        $this->template('admin', $data);
    }

    function set_limit() {
        $limit = $this->url->segment(3);

        if (!is_numeric($limit)) {
            die();
        }

        $_SESSION['limit'] = $limit;
        redirect(BASE_URL.'enquiries/manage');
    }

    function _fetch_rows($return_total_rows = NULL) {

        $search_phrase = urldecode($this->url->segment(4));

        $params['today'] = date('Y-m-d H:i:s');
        $sql = 'select * from enquiries WHERE date_and_time_created<:today order by date_and_time_created desc';


        if (!isset($return_total_rows)) {
            $limit = $this->_get_limit();
            $offset = $this->_get_offset();
            $sql = $sql.' limit '.$offset.', '.$limit;
        }

        if (isset($params)) {
            $result = $this->model->query_bind($sql, $params, 'object');
            if (count($result) < 1) {
                redirect('enquiries/not_found/'.urlencode($search_phrase));
            }
        } else {
            $result = $this->model->query($sql, 'object');
        }

        if (isset($return_total_rows)) {
            $result = count($result);
        }

        return $result;
    }

    function _get_offset() {
        $offset = $this->url->segment(3);
        $limit = $this->_get_limit();

        if (!is_numeric($offset)) {
            $offset = 0;
        } else {
            $offset = ($offset - 1) * $limit;
            if ($offset < 0) {
                $offset = 0;
            }
        }

        return $offset;
    }

    function _get_limit() {

        if (isset($_SESSION['limit'])) {
            $limit = $_SESSION['limit'];
        } else {
            $limit = 10;

        }

        return $limit;
    }

    function show() {
        $this->module('security');
        $token = $this->security->_make_sure_allowed();

        $update_id = $this->url->segment(3);

        if ((!is_numeric($update_id)) && ($update_id != '')) {
            redirect('enquiries/manage');
        }

        $data = $this->_get_data_from_db($update_id);
        $data['token'] = $token;

        if ($data == false) {
            redirect('enquiries/manage');
        } else {
            $data['form_location'] = BASE_URL.'enquiries/submit/'.$update_id;
            $data['update_id'] = $update_id;
            $data['headline'] = 'Enquiry Information';
            $data['date_and_time_created'] = $this->_datetime_to_words($data['date_and_time_created']);
            $data['opened'] = $this->_boolean_to_words($data['opened']);
            $data['view_file'] = 'show';
            $this->template('admin', $data);
        }
    }

    function _get_rankings_options($selected_key) {

        if ($selected_key == '') {
            $options[''] = 'Select...';
        }
        
        $sql = "select * from rankings order by rating_title";
        $rows = $this->model->query($sql, 'object');

        foreach ($rows as $row) {
            $row_desc = $row->rating_title;;
            $options[$row->id] = $row_desc;
        }

        if ($selected_key>0) {
            $row_label = $options[$selected_key];
            $options[0] = strtoupper('*** Disassociate with '.$row_label.' ***');
        }

        return $options;
    }

    function create() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $update_id = $this->url->segment(3);
        $submit = $this->input('submit', true);

        if ((!is_numeric($update_id)) && ($update_id != '')) {
            redirect('enquiries/manage');
        }

        //fetch the form data
        if (($submit == '') && ($update_id > 0)) {
            $data = $this->_get_data_from_db($update_id);
            $data['date_and_time_created'] = date('m/d/Y \a\t H:i', strtotime($data['date_and_time_created']));
        } else {
            $data = $this->_get_data_from_post();
        }

        if ($data['rankings_id'] == 0) {
            $data['rankings_id'] = '';
        }

        $data['rankings_options'] = $this->_get_rankings_options($data['rankings_id']);
        $data['headline'] = $this->_get_page_headline($update_id);

        if ($update_id > 0) {
            $data['cancel_url'] = BASE_URL.'enquiries/show/'.$update_id;
            $data['btn_text'] = 'UPDATE ENQUIRY DETAILS';
        } else {
            $data['cancel_url'] = BASE_URL.'enquiries/manage';
            $data['btn_text'] = 'CREATE ENQUIRY RECORD';
        }

        $additional_includes_top[] = 'https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css';
        $additional_includes_top[] = 'https://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.css';
        $additional_includes_top[] = 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"';
        $additional_includes_top[] = 'https://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.js';
        $additional_includes_top[] = BASE_URL.'admin_files/js/i18n/jquery-ui-timepicker-addon-i18n.min.js';
        $additional_includes_top[] = BASE_URL.'admin_files/js/jquery-ui-sliderAccess.js';
        $data['additional_includes_top'] = $additional_includes_top;

        $data['form_location'] = BASE_URL.'enquiries/submit/'.$update_id;
        $data['update_id'] = $update_id;
        $data['view_file'] = 'create';
        $this->template('admin', $data);
    }

    function _get_page_headline($update_id) {
        //figure out what the page headline should be (on the enquiries/create page)
        if (!is_numeric($update_id)) {
            $headline = 'Create New Enquiry Record';
        } else {
            $headline = 'Update Enquiry Details';
        }

        return $headline;
    }

    function submit() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $submit = $this->input('submit', true);

        if ($submit == 'Submit') {

            $this->validation_helper->set_rules('date_and_time_created', 'Date And Time Created', 'required|valid_datetimepicker_us');
            $this->validation_helper->set_rules('information', 'Information', 'required|min_length[2]');

            $result = $this->validation_helper->run();

            if ($result == true) {

                $update_id = $this->url->segment(3);
                $data = $this->_get_data_from_post();
                settype($data['opened'], 'int');

                //convert date picker into db friendly format
                $data['date_and_time_created'] = str_replace(' at ', '', $data['date_and_time_created']);
                $data['date_and_time_created'] = date('Y-m-d H:i:s', strtotime($data['date_and_time_created']));


                if (is_numeric($update_id)) {
                    //update an existing record

if (!is_numeric($data['rankings_id'])) {
    $data['rankings_id'] = 0;
}

                    $this->model->update($update_id, $data, 'enquiries');
                    $flash_msg = 'The record was successfully updated';

                } else {
                    //insert the new record
                    $update_id = $this->model->insert($data, 'enquiries');
                    $flash_msg = 'The record was successfully created';
                }

                set_flashdata($flash_msg);
                redirect('enquiries/show/'.$update_id);

            } else {
                //form submission error
                $this->create();
            }

        }

    }

    function submit_delete() {
        $this->module('security');
        $this->security->_make_sure_allowed();

        $submit = $this->input('submit', true);

        if ($submit == 'Submit') {
            $update_id = $this->url->segment(3);

            if (!is_numeric($update_id)) {
                die();
            } else {
                $data['update_id'] = $update_id;

                //delete all of the comments associated with this record
                $sql = 'delete from comments where target_table = :module and update_id = :update_id';
                $data['module'] = $this->module;
                $this->model->query_bind($sql, $data);

                //delete the record
                $this->model->delete($update_id, $this->module);

                //set the flashdata
                $flash_msg = 'The record was successfully deleted';
                set_flashdata($flash_msg);

                //redirect to the manage page
                redirect('enquiries/manage');
            }
        }
    }

    function _get_data_from_db($update_id) {
        $enquiries = $this->model->get_where($update_id, 'enquiries');

        if ($enquiries == false) {
            $this->template('error_404');
            die();
        } else {
            $data['date_and_time_created'] = $enquiries->date_and_time_created;
            $data['information'] = $enquiries->information;
            $data['opened'] = $enquiries->opened;
            $data['rankings_id'] = $enquiries->rankings_id;
            return $data;
        }
    }

    function _get_data_from_post() {
        $data['date_and_time_created'] = $this->input('date_and_time_created', true);
        $data['information'] = $this->input('information', true);
        $data['rankings_id'] = $this->input('rankings_id', true);
        return $data;
    }

    function _datetime_to_words($date) {
        $date = date('l, F jS Y \a\t g:i:s A', strtotime($date));
        return $date;
    }

    function _boolean_to_words($value) {
        if ($value == 1) {
            $value = 'yes';
        } else {
            $value = 'no';
        }
        return $value;
    }

    function _prep_output($output) {
        $output['body'] = json_decode($output['body']);
        foreach($output['body'] as $key => $value) {
            $output['body'][$key] ->date_and_time_created = $this->_datetime_to_words($value->date_and_time_created);
            $output['body'][$key] ->opened = $this->_boolean_to_words($value->opened);
        }

        $output['body'] = json_encode($output['body']);

        return $output;
    }

}