<div class="w3-row">
	<div class="w3-container">
		<h1>Not Found</h1>
		<p>Your search produced no results.</p>
		<p><a href="<?= BASE_URL ?>enquiries/manage"><button class="w3-button w3-medium w3-white w3-border"><i class="fa fa-arrow-left"></i> GO BACK</button></a></p>
	</div>
</div>