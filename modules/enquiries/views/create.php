<p><h1><?= $headline ?></h1>
<?= validation_errors() ?>
<div class="w3-card-4">
    <div class="w3-container primary">
        <h4>Enquiry Details</h4>
    </div>
    <form class="w3-container" action="<?= $form_location ?>" method="post">

        <p>
            <label class="w3-text-dark-grey"><b>Date And Time Created</b></label>
            <input autocomplete="off" type="text" name="date_and_time_created" value="<?= $date_and_time_created ?>" class="w3-input w3-border w3-sand datetimepicker" placeholder="Select Date And Time Created">
        </p>
        <p>
            <label class="w3-text-dark-grey"><b>Information</b></label>
            <textarea name="information" class="w3-input w3-border w3-sand" placeholder="Enter Information here..."><?= $information ?></textarea>
        </p>
        <p>
            <label class="w3-text-dark-grey"><b>Rankings ID</b></label>
            <?php
            $attributes['class'] = 'w3-select w3-border w3-sand';
            echo form_dropdown('rankings_id', $rankings_options, $rankings_id, $attributes);
            ?>
        </p>
        <p> 
            <?php 
            $attributes['class'] = 'w3-button w3-white w3-border';
            echo anchor($cancel_url, 'CANCEL', $attributes);
            ?> 
            <button type="submit" name="submit" value="Submit" class="w3-button w3-medium primary"><?= $btn_text ?></button>
        </p>
    </form>
</div>

<script>
$('.datepicker').datepicker();
$('.datetimepicker').datetimepicker({
    separator: ' at '
});
$('.timepicker').timepicker();
</script>