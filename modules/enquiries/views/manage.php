<div class="w3-row">
	<div class="w3-container">
		<h1><?= $headline ?></h1>

        <p><?php 
            if (isset($include_view_all_btn)) { ?>
                <a href="<?= BASE_URL ?>enquiries/manage"><button class="w3-button w3-medium w3-white w3-border"><i class="fa fa-arrow-left"></i> VIEW ALL RECORDS</button></a> <?php 
            }
            ?><a href="<?= BASE_URL ?>enquiries/create"><button class="w3-button w3-medium primary">
                <i class="fa fa-pencil"></i> CREATE NEW RECORD</button></a>
        </p>
		<?= Pagination::display($data) ?>
        
		<?php 
		if (count($rows)>0) { ?>
        <table class="w3-table results-tbl">
            <thead>
                <tr class="primary">
                	<th colspan="6">
                        <div class="table-top">
                            <div class="w3-left">
                            	<?= form_open('enquiries/submit_search') ?>
                                <input type="text" name="search_phrase" placeholder="Search records...">
                                <button name="submit" value="Search" type="submit" onclick="submitSearch()"><i class="fa fa-search"> Search</i></button>
                                <?= form_close() ?>
                            </div>
                            <div class="w3-right">Records Per Page:
                                <div class="w3-dropdown-click">
                                    <button id="perPage" onclick="togglePerPage()"><?= $limit ?></button>
                                    <div id="per-page-options" class="w3-dropdown-content w3-bar-block w3-border" style="right:0">
                                        <a href="#" class="w3-bar-item w3-button" onClick="setPerPage(10)">10</a>
                                        <a href="#" class="w3-bar-item w3-button" onClick="setPerPage(20)">20</a>
                                        <a href="#" class="w3-bar-item w3-button" onClick="setPerPage(50)">50</a>
                                        <a href="#" class="w3-bar-item w3-button" onClick="setPerPage(100)">100</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </th>
                </tr>
                <tr class="secondary">
                    <th style="width: 45px;">&nbsp;</th>
                    <th>Ranking</th>
                    <th>Date And Time Created</th>
                    <th>Preview</th>
                    <th>Opened</th>
                    <th style="width: 20px;">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            foreach($rows as $row) {
                $target_url = BASE_URL.'enquiries/show/'.$row->id;
                if ($row->opened>0) {
                    $opened_status = 'opened';
                    $row_icon = '<i class="fa fa-envelope-open-o"></i>';
                } else {
                    $opened_status = 'unopened';
                    $target_url = str_replace('/show/', '/open_enquiry/', $target_url);
                    $row_icon = '<i class="fa fa-envelope" style="color: gold;"></i>';
                }
        	?>
                <tr>
                    <td><?= $row_icon ?></td>
                    <td><?= $rankings[$row->rankings_id] ?></td>
                    <td><?= $row->date_and_time_created ?></td>
                    <td><?= $row->information ?></td>
                    <td><?= $opened_status ?></td>
                    <td style="width: 20px;">
					    <a href="<?= $target_url ?>"><button type="button" class="btn btn-xs">View</button></a>
                    </td>
                </tr>
        	<?php
        	}
        	?> 	
            </tbody>
        </table>
        <p>
        <?php
        unset($data['include_showing_statement']);
        echo Pagination::display($data);
        }
        ?>
        </p>
	</div>
</div>

<style>
    .pagination {
      display: inline-block;
    }

    .pagination a {
      color: black;
      float: left;
      padding: 8px 16px;
      text-decoration: none;
      border: 1px solid #ddd;
    }

    .pagination a.active {
      background-color: #51bf99;
      color: white;
      border: 1px solid #51bf99;
    }

    .pagination a:hover:not(.active) {background-color: #ddd;}

    .pagination a:first-child {
      border-top-left-radius: 5px;
      border-bottom-left-radius: 5px;
    }

    .pagination a:last-child {
      border-top-right-radius: 5px;
      border-bottom-right-radius: 5px;
    }
</style>

<script>
	function togglePerPage() {
	  	var x = document.getElementById("per-page-options");
	  	if (x.className.indexOf("w3-show") == -1) {
	    	x.className += " w3-show";
	  	} else {
	    	x.className = x.className.replace(" w3-show", "");
	  	}
	}


	function setPerPage(perPage) {
		window.location.href = "<?= $set_limit_url ?>" + perPage;
	}
</script>